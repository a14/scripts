#!/bin/bash

img_name=$(echo "$(date +%Y-%m-%d-%H%M%S).png")
directory1=~/Pictures/screenshots/
directory2=/home/www/r337z/shots/
#viewer=viewnior

function postshot {
    mv $img_name $directory1;
    #$viewer $directory1$img_name;
    cp $directory1$img_name $directory2
}

function help {
    
    echo Usage: screenshot [option]
    echo
    echo DESCRIPTION
    echo -e '\t'    screenshot  is  a  screen  capture script using scrot utility to aquire and save images.  screenshot has a few options, detailed below.
    echo 
    echo Options:
    echo -e '\t'    h    display this help and exit.
    echo -e '\t'    m    For multiple heads, grab shot from each and join them together.
    echo -e '\t'    s    Interactively select a window or rectangle with the mouse.
    echo -e '\t'    w    Use the currently focused window.
    echo
    echo Example:
    echo -e '\t'    screenshot s
    echo -e '\t'    Lets you select a window or rectangle with the mouse, Creates a file called something like 2000-10-30-193402.png and moves it to your images directory.

}

if [ "$1" == "" ]
then
    scrot $img_name;
    postshot
elif [ "$1" == "w" ]
then
    scrot -u $img_name;
    postshot
elif [ "$1" == "s" ]
then
    scrot -s $img_name;
    postshot
elif [ "$1" == "m" ]
then
    scrot -m $img_name;
    postshot
elif [ "$1" == "h" ]
then
    help
else
    echo "error"
fi
